# Projet Web L1

**Table des contenus:**

- Description
- Statut du projet
- IDE utilisé
- Droit d'auteurs


**Description**

Projet web datant de la première année de licence sur le thème du contenu personnalisé du jeu "Les sims 4" . La totalité de ce projet est réalisé en HTML CSS.

**Statut du projet**

Le projet a été terminé le 08 Mai 2021.

**IDE utilisé**

Le projet a été réalisé sur Brackets et est à ouvrir sur Brackets.

**Droit d'auteurs**

Le projet a été réalisé dans son intégrité par Marie Nivet.

